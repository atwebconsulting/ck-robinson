window.addEventListener('load', function () {

    // basic settings
    var conditionUrlCs = "https://www.ckrobinson.cz/?o-nas/zpracovani-osobnich-udaju-a-podminky-uziti-webu";
    var domain = "CKRobinson.cz";
    var cssPath = "";

    // obtain cookieconsent plugin
    var cookieconsent = initCookieConsent();    



    // run plugin with config object
    cookieconsent.run({
        autorun: true,
        revision: 1,
        current_lang: document.documentElement.getAttribute('lang'),
        theme_css: cssPath + '/cookieconsent.css',
        autoclear_cookies: true,
        page_scripts: true,
        cookie_expiration: 365,

        gui_options: {
            consent_modal: {
                layout: 'bar',                // box,cloud,bar
                position: 'bottom',           // bottom,middle,top + left,right,center
                transition: 'slide'           // zoom,slide
            },
            settings_modal: {
                layout: 'box',                // box,bar
                transition: 'slide'           // zoom,slide
            }
        },

        onAccept: function (cookie) {
            
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onAccept)!");
        },

        onChange: function (cookie, changed_preferences) {
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onChange)!");
        },

        languages: {
            cs: {
                consent_modal: {
                    title: 'Nastavení cookies',
                    description: 'Aby pro vás bylo prohlížení našich stránek příjemnější a byli jste stále v obraze o našich službách, používáme různé cookies. Jsou to jak nutné cookies (nezbytné pro správné fungování webu), tak analytické (pomáhající nám zlepšovat náš web dle vašich preferencí) a marketingové (umožňující nám oslovit vás relevantní nabídkou našich služeb na stránkách partnerů jako je Facebook nebo Seznam). K jejich využití potřebujeme váš souhlas, který dáte kliknutím na <strong>Přijmout všechny</strong>.<br><br>Svůj souhlas můžete kdykoliv odvolat, příp. nastavit souhlas jen s některými z nich kliknutím na "Nastavit souhlasy", či <a id="nastavitnutne" type="button"  class="cc-link">přijmout jen nezbytné</a> . <br><br> Více o zpracování osobních údajů na našich stránkách <a href="' + conditionUrlCs + '" target="_new">naleznete zde</a>.',
                    primary_btn: {
                        text: 'Přijmout všechny',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Nastavit souhlasy',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Nastavení cookies',
                    save_settings_btn: 'Uložit nastavení',
                    accept_all_btn: 'Přijmout vše',
                    cookie_table_headers: [
                        {col1: 'Jméno cookie'},
                        {col2: 'Doména/Služba'},
                        {col3: 'Expirace'},
                    ],
                    blocks: [
                        {
                            title: 'Použité cookies',
                            description: 'Na webu používáme cookies. Můžete si vybrat, které cookies povolíte.'
                        }, {
                            title: 'Nutné cookies',
                            description: 'Tyto cookies jsou opravdu potřebné pro základní funkčnost webu. Bez nich by web nemusel fungovat správně.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_cookie',
                                    col2: domain,
                                    col3: '12 měsíců',
                                }
                            ]
                        }, 
                        {
                            title: 'Analytické cookies',
                            description: 'Tyto cookies sbírají informace o průchodu návštěvníka našimi stránkami . Všechny tyto cookies jsou anonymní a nemohu návštěvníka nijak identifikovat. Tyto cookies nejsou nutné pro fungování webu jako takového, ale pomáhají nám web pro vás neustále vylepšovat.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: '^_ga',
                                    col2: 'Google Analytics',
                                    col3: '2 roky',
                                    is_regex: true
                                },
                                {
                                    col1: '_gid',
                                    col2: 'Google Analytics',
                                    col3: '1 den',
                                },
                                {
                                    col1: '_gat',
                                    col2: 'Google Analytics',
                                    col3: '1 den',
                                }
                            ]
                        },
                        {
                            title: 'Marketingové cookies',
                            description: 'Cílem těchto cookies je propojit náš web se sociálními a reklamními sítěmi třetích stran jako například Facebook nebo Google Ads. Díky tomuto propojení vám u těchto partnerů může být zobrazována relevantní reklama.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [  
                                {
                                    col1: 'sid',
                                    col2: 'Seznam.cz, a.s.',
                                    col3: '1 měsíc',
                                },
                                {
                                    col1: 'APNUID',
                                    col2: 'Seznam.cz, a.s.',
                                    col3: '6 měsíců',
                                },
                                {
                                    col1: 'KADUSERCOOKIE',
                                    col2: 'Seznam.cz, a.s.',
                                    col3: '6 měsíců',
                                }
                            ]
                        },                       
                        {
                            title: 'Více informací',
                            description: 'Více informací o zpracování osobních údajů na našich stránkách najdete <a class="cc-link" target="_blank" href="' + conditionUrlCs + '">zde</a>.',
                        }
                    ]
                }
            },            
            
        }
    });


    var nastavitNutne = document.getElementById("nastavitnutne");

    if( nastavitNutne ){

        nastavitNutne.onclick = function () {
            cookieconsent.hide();
            cookieconsent.accept([]); 
        };

    }    

}); 